package com.example.sergii.rpimonitor;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.app.NotificationCompat;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {


    NavigationView navigationView = null;
    Toolbar toolbar = null;
    private TextView tvTemp;
    private TextView tvHum;
    private TextView tvPress;
    public String[] cValues = new String[3];

    NotificationCompat.Builder notification;
    private static final int uniqueID = 1;
    CurrentValuesFragment cvFragment = new CurrentValuesFragment();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //set the fragment

        android.support.v4.app.FragmentTransaction fragmentTransaction =
                getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.fragment_container, cvFragment);
        fragmentTransaction.commit();


        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        Button button = (Button) findViewById(R.id.button);


        getData();
        notification = new NotificationCompat.Builder(this);
        notification.setAutoCancel(true);

    }
    public void getData(){
        new JSONCurrentValues().execute("http://192.168.0.14/android_connect/get_latest_values.php");
        cvFragment.setText();
    }

    public void buttonClicked(View view){
        getData();
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement


        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_current_values) {
            //set the fragment
            CurrentValuesFragment cvFragment = new CurrentValuesFragment();
            android.support.v4.app.FragmentTransaction fragmentTransaction =
                    getSupportFragmentManager().beginTransaction();
            fragmentTransaction.replace(R.id.fragment_container, cvFragment);
            fragmentTransaction.commit();
        } else if (id == R.id.nav_trend_line) {
            //set the fragment
            TrendFormulaFragment tfFragment = new TrendFormulaFragment();
            android.support.v4.app.FragmentTransaction fragmentTransaction =
                    getSupportFragmentManager().beginTransaction();
            fragmentTransaction.replace(R.id.fragment_container, tfFragment);
            fragmentTransaction.commit();

        } else if (id == R.id.nav_about) {
            AboutFragment aboutFragment = new AboutFragment();
            android.support.v4.app.FragmentTransaction fragmentTransaction =
                    getSupportFragmentManager().beginTransaction();
            fragmentTransaction.replace(R.id.fragment_container,aboutFragment);
            fragmentTransaction.commit();
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
    public class JSONCurrentValues extends AsyncTask<String, String, String[]> {
        @Override
        public String[] doInBackground(String... params){
            HttpURLConnection urlConnection = null;
            BufferedReader bufferedReader = null;
            try {
                URL urlTemp = new URL(params[0]);
                urlConnection = (HttpURLConnection) urlTemp.openConnection();
                urlConnection.setRequestMethod("POST");
                urlConnection.connect();

                bufferedReader = new BufferedReader((new InputStreamReader(urlConnection.getInputStream())));
                String next="";
                StringBuffer buffer = new StringBuffer();
                while ((next = bufferedReader.readLine()) != null) {
                    buffer.append(next);
                }
                String finalJSON = buffer.toString();
                JSONObject parentObject = new JSONObject(finalJSON);
                JSONArray parentArray = parentObject.getJSONArray("values");
                JSONObject finalObject = parentArray.getJSONObject(0);
                String tempValue = finalObject.getString("temperature");
                String humValue = finalObject.getString("humidity");
                String pressValue = finalObject.getString("pressure");
                String [] resultArray = new String[3];
                resultArray[0]=tempValue;
                resultArray[1]=humValue;
                resultArray[2]=pressValue;

                return resultArray;


            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            } finally {
                if (urlConnection!=null){
                    urlConnection.disconnect();
                }
                try {
                    if(bufferedReader!=null){
                        bufferedReader.close();
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(String[] result) {
            float fTemp;
            float fHum;
            float fPress;

            super.onPostExecute(result);

            MainActivity.this.cValues[0]=result[0];
            MainActivity.this.cValues[1]=result[1];
            MainActivity.this.cValues[2]=result[2];
            //tvTemp.setText(result[0]+" "+(char) 0x00B0+"C");
            //tvHum.setText(result[1]+" %");
            //tvPress.setText(result[2]+" mmHg");

            fTemp = Float.parseFloat(result[0]);
            fHum = Float.parseFloat(result[1]);


            //Check the values of Temperature and send notifications
            if ((fTemp>=24.0)&&(fTemp<30.0)){
                notification.setSmallIcon(R.mipmap.ic_launcher);
                notification.setTicker("WARNING! Temperature is higher than normal!");
                notification.setWhen(System.currentTimeMillis());
                notification.setContentTitle("WARNING! Temperature is higher than normal!");
                notification.setContentText("Temperature is higher than normal. Please turn on Air Conditioner!");

                Intent intent = new Intent(MainActivity.this, MainActivity.class);
                PendingIntent pendingIntent = PendingIntent.getActivity(MainActivity.this, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
                notification.setContentIntent(pendingIntent);

                //builds notification and issues it
                NotificationManager nm = (NotificationManager)getSystemService(NOTIFICATION_SERVICE);
                nm.notify(uniqueID, notification.build());
            } else if (fTemp>=30.0){
                notification.setSmallIcon(R.mipmap.ic_launcher);
                notification.setTicker("CRITICAL! Temperature is CRITICAL.");
                notification.setWhen(System.currentTimeMillis());
                notification.setContentTitle("CRITICAL! Temperature is CRITICAL!");
                notification.setContentText("Temperature is CRITICAL! Please turn on Air Conditioner!");

                Intent intent = new Intent(MainActivity.this, MainActivity.class);
                PendingIntent pendingIntent = PendingIntent.getActivity(MainActivity.this, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
                notification.setContentIntent(pendingIntent);

                //builds notification and issues it
                NotificationManager nm = (NotificationManager)getSystemService(NOTIFICATION_SERVICE);
                nm.notify(uniqueID, notification.build());
            } else if ((fTemp<20.0)&&(fTemp>=18.0)){
                notification.setSmallIcon(R.mipmap.ic_launcher);
                notification.setTicker("WARNING! Temperature is lower than normal!");
                notification.setWhen(System.currentTimeMillis());
                notification.setContentTitle("WARNING! Temperature is lower than normal!");
                notification.setContentText("Temperature is lower than normal. Please turn on Air Conditioner!");

                Intent intent = new Intent(MainActivity.this, MainActivity.class);
                PendingIntent pendingIntent = PendingIntent.getActivity(MainActivity.this, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
                notification.setContentIntent(pendingIntent);

                //builds notification and issues it
                NotificationManager nm = (NotificationManager)getSystemService(NOTIFICATION_SERVICE);
                nm.notify(uniqueID, notification.build());
            } else if ((fTemp<18.0)&&(fTemp>=12.0)){
                notification.setSmallIcon(R.mipmap.ic_launcher);
                notification.setTicker("CRITICAL! Temperature is CRITICAL.");
                notification.setWhen(System.currentTimeMillis());
                notification.setContentTitle("CRITICAL! Temperature is CRITICAL!");
                notification.setContentText("Temperature is CRITICAL! Please turn on Air Conditioner!");

                Intent intent = new Intent(MainActivity.this, MainActivity.class);
                PendingIntent pendingIntent = PendingIntent.getActivity(MainActivity.this, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
                notification.setContentIntent(pendingIntent);

                //builds notification and issues it
                NotificationManager nm = (NotificationManager)getSystemService(NOTIFICATION_SERVICE);
                nm.notify(uniqueID, notification.build());
            } else if (fTemp<12.0){
                notification.setSmallIcon(R.mipmap.ic_launcher);
                notification.setTicker("DANGER! Temperature is too low!");
                notification.setWhen(System.currentTimeMillis());
                notification.setContentTitle("DANGER! Temperature is too low!");
                notification.setContentText("Temperature is too low. Please turn on Air Conditioner!");

                Intent intent = new Intent(MainActivity.this, MainActivity.class);
                PendingIntent pendingIntent = PendingIntent.getActivity(MainActivity.this, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
                notification.setContentIntent(pendingIntent);

                //builds notification and issues it
                NotificationManager nm = (NotificationManager)getSystemService(NOTIFICATION_SERVICE);
                nm.notify(uniqueID, notification.build());
            } else{
                System.out.println("fTemp is ok!");
            }

            //Check the values of Humidity and send notification
            if ((fHum<30.0)&&(fHum>20.0)){
                notification.setSmallIcon(R.mipmap.ic_launcher);
                notification.setTicker("WARNING! Himidity is lower than normal!");
                notification.setWhen(System.currentTimeMillis());
                notification.setContentTitle("WARNING! Himidity is lower than normal!");
                notification.setContentText("Humidity is lower than normal. Please turn on Humidifier!");

                Intent intent = new Intent(MainActivity.this, MainActivity.class);
                PendingIntent pendingIntent = PendingIntent.getActivity(MainActivity.this, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
                notification.setContentIntent(pendingIntent);

                //builds notification and issues it
                NotificationManager nm = (NotificationManager)getSystemService(NOTIFICATION_SERVICE);
                nm.notify(uniqueID, notification.build());

            } else if (fHum<20.0){
                notification.setSmallIcon(R.mipmap.ic_launcher);
                notification.setTicker("CRITICAL! Humidity is CRITICAL.");
                notification.setWhen(System.currentTimeMillis());
                notification.setContentTitle("CRITICAL! Humidity is CRITICAL!");
                notification.setContentText("Temperature is CRITICAL! Please turn on Humidifier!");

                Intent intent = new Intent(MainActivity.this, MainActivity.class);
                PendingIntent pendingIntent = PendingIntent.getActivity(MainActivity.this, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
                notification.setContentIntent(pendingIntent);

                //builds notification and issues it
                NotificationManager nm = (NotificationManager)getSystemService(NOTIFICATION_SERVICE);
                nm.notify(uniqueID, notification.build());
            } else if ((fHum>50.0)&&(fHum<=60.0)){
                notification.setSmallIcon(R.mipmap.ic_launcher);
                notification.setTicker("WARNING! Himidity is higher than normal!");
                notification.setWhen(System.currentTimeMillis());
                notification.setContentTitle("WARNING! Himidity is higher than normal!");
                notification.setContentText("Humidity is higher than normal. Please turn on Humidifier!");

                Intent intent = new Intent(MainActivity.this, MainActivity.class);
                PendingIntent pendingIntent = PendingIntent.getActivity(MainActivity.this, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
                notification.setContentIntent(pendingIntent);

                //builds notification and issues it
                NotificationManager nm = (NotificationManager)getSystemService(NOTIFICATION_SERVICE);
                nm.notify(uniqueID, notification.build());
            } else if (fHum>60.0){
                notification.setSmallIcon(R.mipmap.ic_launcher);
                notification.setTicker("CRITICAL! Humidity is CRITICAL.");
                notification.setWhen(System.currentTimeMillis());
                notification.setContentTitle("CRITICAL! Humidity is CRITICAL!");
                notification.setContentText("Temperature is CRITICAL! Please turn on Humidifier!");

                Intent intent = new Intent(MainActivity.this, MainActivity.class);
                PendingIntent pendingIntent = PendingIntent.getActivity(MainActivity.this, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
                notification.setContentIntent(pendingIntent);

                //builds notification and issues it
                NotificationManager nm = (NotificationManager)getSystemService(NOTIFICATION_SERVICE);
                nm.notify(uniqueID, notification.build());
            } else {
                System.out.println("fHum is OK!");
            }



        }
    }
}
