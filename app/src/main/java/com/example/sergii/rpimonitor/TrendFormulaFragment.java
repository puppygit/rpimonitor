package com.example.sergii.rpimonitor;


import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;


/**
 * A simple {@link Fragment} subclass.
 */
public class TrendFormulaFragment extends Fragment {

    public TextView tempFormula;
    public TextView humFormula;
    public TextView pressFormula;
    public String tempTrend;
    public String humTrend;
    public String presTrend;

    public TrendFormulaFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View trendFormulasView = inflater.inflate(R.layout.fragment_trend_formula, container, false);
        tempFormula = (TextView)trendFormulasView.findViewById(R.id.textView9);
        humFormula = (TextView)trendFormulasView.findViewById(R.id.textView11);
        pressFormula = (TextView)trendFormulasView.findViewById(R.id.textView13);

        getFormulas();
        return trendFormulasView;
    }

    public void getFormulas(){
        new JSONTempFormulaTask().execute("http://192.168.0.14/android_connect/getTempFormula.php");
        new JSONHumFormulaTask().execute("http://192.168.0.14/android_connect/getHumFormula.php");
        new JSONPresFormulaTask().execute("http://192.168.0.14/android_connect/getPresFormula.php");

    }
    //Fetching Data Async Tasks Classes

    public class JSONTempFormulaTask extends AsyncTask<String, String, String> {
        @Override
        public String doInBackground(String... params){
            HttpURLConnection urlConnection = null;
            BufferedReader bufferedReader = null;
            try {
                URL urlTemp = new URL(params[0]);
                urlConnection = (HttpURLConnection) urlTemp.openConnection();
                urlConnection.setRequestMethod("POST");
                urlConnection.connect();

                bufferedReader = new BufferedReader((new InputStreamReader(urlConnection.getInputStream())));
                String next="";
                StringBuffer buffer = new StringBuffer();
                while ((next = bufferedReader.readLine()) != null) {
                    buffer.append(next);
                }
                String finalJSON = buffer.toString();
                JSONObject parentObject = new JSONObject(finalJSON);
                JSONArray parentArray = parentObject.getJSONArray("values");
                JSONObject finalObject = parentArray.getJSONObject(0);
                String temFormula = finalObject.getString("tempFormula");

                return temFormula;


            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            } finally {
                if (urlConnection!=null){
                    urlConnection.disconnect();
                }
                try {
                    if(bufferedReader!=null){
                        bufferedReader.close();
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            tempTrend=result;
            tempFormula.setText(tempTrend);
        }
    }
    public class JSONHumFormulaTask extends AsyncTask<String, String, String> {
        @Override
        public String doInBackground(String... params){
            HttpURLConnection urlConnection = null;
            BufferedReader bufferedReader = null;
            try {
                URL urlTemp = new URL(params[0]);
                urlConnection = (HttpURLConnection) urlTemp.openConnection();
                urlConnection.setRequestMethod("POST");
                urlConnection.connect();

                bufferedReader = new BufferedReader((new InputStreamReader(urlConnection.getInputStream())));
                String next="";
                StringBuffer buffer = new StringBuffer();
                while ((next = bufferedReader.readLine()) != null) {
                    buffer.append(next);
                }
                String finalJSON = buffer.toString();
                JSONObject parentObject = new JSONObject(finalJSON);
                JSONArray parentArray = parentObject.getJSONArray("values");
                JSONObject finalObject = parentArray.getJSONObject(0);
                String hFormula = finalObject.getString("humFormula");

                return hFormula;


            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            } finally {
                if (urlConnection!=null){
                    urlConnection.disconnect();
                }
                try {
                    if(bufferedReader!=null){
                        bufferedReader.close();
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            humTrend=result;
            humFormula.setText(humTrend);
        }
    }
    public class JSONPresFormulaTask extends AsyncTask<String, String, String> {
        @Override
        public String doInBackground(String... params){
            HttpURLConnection urlConnection = null;
            BufferedReader bufferedReader = null;
            try {
                URL urlTemp = new URL(params[0]);
                urlConnection = (HttpURLConnection) urlTemp.openConnection();
                urlConnection.setRequestMethod("POST");
                urlConnection.connect();

                bufferedReader = new BufferedReader((new InputStreamReader(urlConnection.getInputStream())));
                String next="";
                StringBuffer buffer = new StringBuffer();
                while ((next = bufferedReader.readLine()) != null) {
                    buffer.append(next);
                }
                String finalJSON = buffer.toString();
                JSONObject parentObject = new JSONObject(finalJSON);
                JSONArray parentArray = parentObject.getJSONArray("values");
                JSONObject finalObject = parentArray.getJSONObject(0);
                String pFormula = finalObject.getString("presFormula");

                return pFormula;


            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            } finally {
                if (urlConnection!=null){
                    urlConnection.disconnect();
                }
                try {
                    if(bufferedReader!=null){
                        bufferedReader.close();
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            presTrend=result;
            pressFormula.setText(presTrend);
        }
    }
}
