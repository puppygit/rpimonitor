package com.example.sergii.rpimonitor;


import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.NotificationCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;


/**
 * A simple {@link Fragment} subclass.
 */
public class CurrentValuesFragment extends Fragment {

    MainActivity mainActivity = (MainActivity)getActivity();
    public TextView tvTemp;
    public TextView tvHum;
    public TextView tvPress;
    public String[] currValues = new String[3];
    //public View currentValuesView;
    public CurrentValuesFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        View currentValuesView = inflater.inflate(R.layout.fragment_current_values, container,false);
        tvTemp = (TextView) currentValuesView.findViewById(R.id.textView3);
        tvHum = (TextView) currentValuesView.findViewById(R.id.textView5);
        tvPress = (TextView) currentValuesView.findViewById(R.id.textView7);
        setText();

        return currentValuesView;
    }
    public void setText(){
        new JSONTask().execute("http://192.168.0.14/android_connect/get_latest_values.php");
        //tvTemp.setText(currValues[0]);
        //tvHum.setText(currValues[1]);
        //tvPress.setText(currValues[2]);
    }
    public class JSONTask extends AsyncTask<String, String, String[]> {
        @Override
        public String[] doInBackground(String... params){
            HttpURLConnection urlConnection = null;
            BufferedReader bufferedReader = null;
            try {
                URL urlTemp = new URL(params[0]);
                urlConnection = (HttpURLConnection) urlTemp.openConnection();
                urlConnection.setRequestMethod("POST");
                urlConnection.connect();

                bufferedReader = new BufferedReader((new InputStreamReader(urlConnection.getInputStream())));
                String next="";
                StringBuffer buffer = new StringBuffer();
                while ((next = bufferedReader.readLine()) != null) {
                    buffer.append(next);
                }
                String finalJSON = buffer.toString();
                JSONObject parentObject = new JSONObject(finalJSON);
                JSONArray parentArray = parentObject.getJSONArray("values");
                JSONObject finalObject = parentArray.getJSONObject(0);
                String tempValue = finalObject.getString("temperature");
                String humValue = finalObject.getString("humidity");
                String pressValue = finalObject.getString("pressure");
                String [] resultArray = new String[3];
                resultArray[0]=tempValue;
                resultArray[1]=humValue;
                resultArray[2]=pressValue;

                return resultArray;


            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            } finally {
                if (urlConnection!=null){
                    urlConnection.disconnect();
                }
                try {
                    if(bufferedReader!=null){
                        bufferedReader.close();
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(String[] result) {
            float fTemp;
            float fHum;
            float fPress;

            super.onPostExecute(result);

            currValues[0]=result[0];
            currValues[1]=result[1];
            currValues[2]=result[2];
            tvTemp.setText(result[0]+" "+(char) 0x00B0+"C");
            tvHum.setText(result[1]+" %");
            tvPress.setText(result[2]+" mmHg");
        }
    }

}
